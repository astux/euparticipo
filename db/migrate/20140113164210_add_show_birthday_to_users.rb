class AddShowBirthdayToUsers < ActiveRecord::Migration
  def change
    add_column :users, :show_birthday, :boolean, default: true
    
    User.update_all(show_birthday: true)
  end
end
