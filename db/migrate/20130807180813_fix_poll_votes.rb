class FixPollVotes < ActiveRecord::Migration
  def change
    drop_table :poll_votes
    
    create_table :poll_votes do |t|
      t.references :user, index: true
      t.references :poll, index: true
      t.boolean :yes
      t.boolean :no
      t.boolean :absent

      t.timestamps
    end
  end
end
