class CreateUserNotifications < ActiveRecord::Migration
  def change
    create_table :user_notifications do |t|
      t.integer :user_id
      t.integer :action_id
      t.integer :origin_id
      t.integer :entity_id

      t.timestamps
    end
  end
end
