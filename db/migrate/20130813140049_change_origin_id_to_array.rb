class ChangeOriginIdToArray < ActiveRecord::Migration
  def change
    remove_column :user_notifications, :origin_id
    add_column :user_notifications, :origin_ids, :integer, array: true, default: []
  end
end
