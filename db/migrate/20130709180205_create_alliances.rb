class CreateAlliances < ActiveRecord::Migration
  def change
    create_table :alliances do |t|
      t.integer :citizen_id
      t.integer :politician_id

      t.timestamps
    end
  end
end
