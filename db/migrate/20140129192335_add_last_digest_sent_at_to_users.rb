class AddLastDigestSentAtToUsers < ActiveRecord::Migration
  def change
    add_column :users, :last_digest_sent_at, :datetime
  end
end
