class FixPollTable < ActiveRecord::Migration
  def change
    drop_table :polls
    
    create_table :polls do |t|
      t.references :post, index: true

      t.timestamps
    end
  end
end
