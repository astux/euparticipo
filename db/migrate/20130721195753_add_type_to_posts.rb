class AddTypeToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :post_type, :string
    add_column :posts, :participe_user_id, :integer
  end
end
