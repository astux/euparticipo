class CreateAbsentPoliticians < ActiveRecord::Migration
  def change
    create_table :absent_politicians do |t|
      t.string :name
      t.string :email
      t.string :political_position
      t.integer :city_id

      t.timestamps
    end
  end
end
