class AddDetailsToUser < ActiveRecord::Migration
  def change
    add_column :users, :name, :string
    add_column :users, :last_names, :string
    add_column :users, :gender, :varchar
    add_column :users, :birthday, :date
    add_column :users, :work_info, :string
    add_column :users, :scholarship, :string
    add_column :users, :relationship_status, :string
    add_column :users, :political_preference, :string
    add_column :users, :about_me, :text
    add_column :users, :favorite_quote, :text
    add_column :users, :biography, :text
    add_column :users, :photo, :string

    add_column :users, :phone1, :string
    add_column :users, :phone2, :string

    add_column :users, :verified, :boolean
    add_column :users, :role, :string
    add_column :users, :political_position, :string

    add_reference :users, :city, index: true
    add_reference :users, :state, index: true

    add_reference :users, :from_city, index: true
    add_reference :users, :from_state, index: true

    add_column :users, :political_city_id, :string
    add_column :users, :political_state_id, :string
  end
end
