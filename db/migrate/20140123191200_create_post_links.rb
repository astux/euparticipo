class CreatePostLinks < ActiveRecord::Migration
  def change
    create_table :post_links do |t|
      t.string :url
      t.string :title
      t.string :image

      t.timestamps
    end
  end
end
