class CreateNudges < ActiveRecord::Migration
  def change
    create_table :nudges do |t|
      t.integer :from_id
      t.integer :to_id

      t.timestamps
    end
  end
end
