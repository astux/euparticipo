class CreatePoliticianInvites < ActiveRecord::Migration
  def change
    create_table :politician_invites do |t|
      t.integer :absent_politician_id
      t.integer :user_id

      t.timestamps
    end
  end
end
