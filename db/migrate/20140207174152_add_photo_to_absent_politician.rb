class AddPhotoToAbsentPolitician < ActiveRecord::Migration
  def change
    add_column :absent_politicians, :photo, :string
  end
end
