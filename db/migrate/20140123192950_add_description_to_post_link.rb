class AddDescriptionToPostLink < ActiveRecord::Migration
  def change
    add_column :post_links, :description, :string
  end
end
