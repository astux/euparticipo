class ChangePostLinkFieldsToUrl < ActiveRecord::Migration
  def change
    change_column :post_links, :description, :text, :limit => nil
  end
end
