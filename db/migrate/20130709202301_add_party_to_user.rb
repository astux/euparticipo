class AddPartyToUser < ActiveRecord::Migration
  def change
    add_reference :users, :party, index: true
  end
end
