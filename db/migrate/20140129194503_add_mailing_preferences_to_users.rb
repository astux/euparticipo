class AddMailingPreferencesToUsers < ActiveRecord::Migration
  def change
    add_column :users, :mail_digest, :boolean, default: true
    add_column :users, :mail_nudge, :boolean, default: true
    
    User.update_all(mail_digest: true)
    User.update_all(mail_nudge: true)
  end
end
