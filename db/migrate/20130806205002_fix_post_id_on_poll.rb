class FixPostIdOnPoll < ActiveRecord::Migration
  def change
    remove_column :polls, :post_id
    add_column :polls, :post_id, :integer
  end
end
