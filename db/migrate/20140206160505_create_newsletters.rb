class CreateNewsletters < ActiveRecord::Migration
  def change
    create_table :newsletters do |t|
      t.text :content
      t.boolean :published
      t.datetime :published_at

      t.timestamps
    end
  end
end
