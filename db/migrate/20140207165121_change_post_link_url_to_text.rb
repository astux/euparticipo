class ChangePostLinkUrlToText < ActiveRecord::Migration
  def change
    change_column :post_links, :url, :text, :limit => nil
  end
end
