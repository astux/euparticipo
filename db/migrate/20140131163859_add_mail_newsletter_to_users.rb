class AddMailNewsletterToUsers < ActiveRecord::Migration
  def change
    add_column :users, :mail_newsletter, :boolean, default: true
    
    User.update_all(mail_newsletter: true)
  end
end
