class CreatePollVotes < ActiveRecord::Migration
  def change
    create_table :poll_votes do |t|
      t.integer :user_id
      t.string :poll_id
      t.string :integer
      t.boolean :yes
      t.boolean :no
      t.boolean :absent

      t.timestamps
    end
  end
end
