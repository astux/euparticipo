# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140207174152) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "absent_politicians", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "political_position"
    t.integer  "city_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo"
  end

  create_table "alliances", force: true do |t|
    t.integer  "citizen_id"
    t.integer  "politician_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cities", force: true do |t|
    t.string   "name"
    t.integer  "state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cities", ["state_id"], name: "index_cities_on_state_id", using: :btree

  create_table "comments", force: true do |t|
    t.integer  "post_id"
    t.integer  "user_id"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comments", ["post_id"], name: "index_comments_on_post_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "follows", force: true do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "likes", force: true do |t|
    t.integer  "post_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "likes", ["post_id"], name: "index_likes_on_post_id", using: :btree
  add_index "likes", ["user_id"], name: "index_likes_on_user_id", using: :btree

  create_table "newsletters", force: true do |t|
    t.text     "content"
    t.boolean  "published"
    t.datetime "published_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "subject"
  end

  create_table "nudges", force: true do |t|
    t.integer  "from_id"
    t.integer  "to_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "parties", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "politician_invites", force: true do |t|
    t.integer  "absent_politician_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "poll_votes", force: true do |t|
    t.integer  "user_id"
    t.integer  "poll_id"
    t.boolean  "yes"
    t.boolean  "no"
    t.boolean  "absent"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "poll_votes", ["poll_id"], name: "index_poll_votes_on_poll_id", using: :btree
  add_index "poll_votes", ["user_id"], name: "index_poll_votes_on_user_id", using: :btree

  create_table "polls", force: true do |t|
    t.integer  "post_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "polls", ["post_id"], name: "index_polls_on_post_id", using: :btree

  create_table "post_links", force: true do |t|
    t.text     "url"
    t.string   "title"
    t.string   "image"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description"
  end

  create_table "posts", force: true do |t|
    t.integer  "user_id"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "post_type"
    t.integer  "participe_user_id"
    t.string   "picture"
    t.integer  "post_link_id"
    t.string   "status"
  end

  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "states", force: true do |t|
    t.string   "name"
    t.string   "abbreviation"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_notifications", force: true do |t|
    t.integer  "user_id"
    t.integer  "action_id"
    t.integer  "entity_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "origin_ids", default: [], array: true
  end

  create_table "user_votes", force: true do |t|
    t.integer  "user_id"
    t.integer  "voted_user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                               default: "",   null: false
    t.string   "encrypted_password",                  default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                       default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "last_names"
    t.string   "gender",                  limit: nil
    t.date     "birthday"
    t.string   "work_info"
    t.string   "scholarship"
    t.string   "relationship_status"
    t.string   "political_preference"
    t.text     "about_me"
    t.text     "favorite_quote"
    t.text     "biography"
    t.string   "photo"
    t.string   "phone1"
    t.string   "phone2"
    t.boolean  "verified"
    t.string   "role"
    t.string   "political_position"
    t.integer  "city_id"
    t.integer  "state_id"
    t.integer  "from_city_id"
    t.integer  "from_state_id"
    t.string   "political_city_id"
    t.string   "political_state_id"
    t.integer  "party_id"
    t.string   "token"
    t.string   "unconfirmed_email"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "political_nickname"
    t.datetime "notifications_read_date"
    t.boolean  "show_birthday",                       default: true
    t.string   "provider"
    t.string   "uid"
    t.string   "facebook_token"
    t.datetime "last_digest_sent_at"
    t.boolean  "mail_digest",                         default: true
    t.boolean  "mail_nudge",                          default: true
    t.boolean  "mail_newsletter",                     default: true
  end

  add_index "users", ["city_id"], name: "index_users_on_city_id", using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["from_city_id"], name: "index_users_on_from_city_id", using: :btree
  add_index "users", ["from_state_id"], name: "index_users_on_from_state_id", using: :btree
  add_index "users", ["party_id"], name: "index_users_on_party_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["state_id"], name: "index_users_on_state_id", using: :btree

end
