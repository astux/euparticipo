class AdminMailer < ActionMailer::Base
  default from: "EuParticipo <contato@euparticipo.com.br>"

  def approve_politician(user)
    @user = user
    mail(to: @user.email, subject: 'EuParticipo: sua conta foi aprovada')
  end
  
  def deny_politician(name, user_email)
    @name = name
    mail(to: user_email, subject: 'EuParticipo: sua conta foi recusada')
  end
end
