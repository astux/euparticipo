class NewsletterMailer < ActionMailer::Base
  layout 'email'
  default from: "newsletter@euparticipo.com.br"
    
  def newsletter(user, newsletter)
    @user = user
    @newsletter = newsletter
    
    mail(to: @user.email, subject: @newsletter.subject)
  end
end
