# -*- coding: utf-8 -*-
class UserMailer < ActionMailer::Base
  default from: "EuParticipo <contato@euparticipo.com.br>"

  def politician_confirmation(user)
    @user = user
    mail(to: @user.email, subject: 'EuParticipo: sua conta foi confirmada')
  end
end
