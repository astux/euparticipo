class ContactMailer < ActionMailer::Base
  default from: "EuParticipo <contato@euparticipo.com.br>"
  
  def user_contact(name, email, subject, message)
    @name = name
    @email = email
    @message = message
    mail(:to => "contato@euparticipo.com.br", :subject => subject, :reply_to => email)
  end
end
