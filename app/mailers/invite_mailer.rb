class InviteMailer < ActionMailer::Base
  layout 'email'
  default from: "EuParticipo <contato@euparticipo.com.br>"

  def politician(invite)
    @invite = invite
    headers['X-SMTPAPI'] = '{"category": "A_Invite"}'
    mail(to: @invite.absent_politician.email, from: 'EuParticipo <contato@euparticipo.com.br>', subject: 'Você foi convidado para o EuParticipo!')
  end
  
  def nudge(nudge)
    @nudge = nudge
    headers['X-SMTPAPI'] = '{"category": "A_Nudge"}'
    mail(to: @nudge.to.email, from: 'EuParticipo <contato@euparticipo.com.br>', subject: "#{@nudge.from.name} chamou sua atenção no EuParticipo!")
  end
end
