class DigestMailer < ActionMailer::Base
  add_template_helper(NotificationsHelper)
  layout 'email'
  default from: "EuParticipo <notificacoes@euparticipo.com.br>"
  
  def digest(user)
    @user = user
    @notifications = WeeklyDigest.notifications_for(@user)
    
    if @notifications.size > 0
      if mail(to: @user.email, subject: 'O que tem acontecido no EuParticipo')
        WeeklyDigest.register_sent_digest(@user)
      end
    end
  end
end
