# encoding: utf-8

class PictureUploader < CarrierWave::Uploader::Base
  include CarrierWave::RMagick

  process :convert => 'jpg'
  process :quality => 85
  process :resize_to_geometry_string => '720>'
  
  version :medium do
    process :resize_to_geometry_string => '400>'
  end
  
  version :square do
    process :resize_to_fill => [140, 140]
  end

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end
end
