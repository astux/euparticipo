class Users::ConfirmationsController < Devise::ConfirmationsController
  layout 'devise'
  
  def after_confirmation_path_for(resource_name, resource)
    root_path(metodo: 'boas-vindas')
  end
end
