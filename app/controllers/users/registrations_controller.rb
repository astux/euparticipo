# -*- coding: utf-8 -*-
class Users::RegistrationsController < Devise::RegistrationsController
  layout 'devise'

  def create
    @user = User.new(sign_up_params)
    @user.photo = params[:user][:photo]

    # 1.
    # No caso de cadastro de um cidadão, "verified" será true
    # automaticamente. Se for um político, virá como false, para que
    # um admin confirme depois.

    # 2.
    # caso um parlamentar esteja tentando se cadastrar,
    # indentificar e então informar à view que ela deve
    # renderizar form de parlamentar, e não de cidadão

    if params[:user][:role] == 'Legislativo' or
        params[:user][:role] == 'Judiciário' or
        params[:user][:role] == 'Executivo' or
        params[:user][:role] == 'Notório'
      params[:user][:verified] = 'f'
      params[:user][:confirmed_at] = DateTime.now
    else params[:user][:role] == 'Cidadão'
      params[:user][:verified] = 't'
    end

    # token

=begin

    name = ''

    if params[:user][:political_nickname]
      name = params[:user][:political_nickname]
    elsif params[:user][:name]
      name = params[:user][:name]
    end

    token = name.parameterize

    users = User.where('token like ?', token + '%')

    if users.length > 0
      tokens = []

      users.each do |u|
        tokens.push u.token
      end

      token_root = token
      suffix = 1

      while tokens.include?(token)
        suffix = suffix.to_i + 1
        token = token_root + suffix.to_s
      end
    end

    params[:user][:token] = token

=end

    super
  end

  protected
  
  def after_sign_up_path_for(resource)
    root_path(metodo: 'boas-vindas')
  end

  private

  def sign_up_params
    params.require(:user).permit(:name,
                                 :birthday,
                                 :gender,
                                 :email,
                                 :verified,
                                 :password,
                                 :password_confirmation,
                                 :last_names,
                                 :work_info,
                                 :scholarship,
                                 :relationship_status,
                                 :political_preference,
                                 :about_me,
                                 :favorite_quote,
                                 :biography,
                                 :verified,
                                 :role,
                                 :political_position,
                                 :political_nickname,
                                 :city_id,
                                 :photo,
                                 :state_id,
                                 :party_id,
                                 :phone1,
                                 :phone2,
                                 :confirmed_at,
                                 :token, 
                                 :political_state_id, 
                                 :political_city_id, 
                                 :provider, 
                                 :uid, 
                                 :facebook_token, 
                                 :remote_photo_url)
  end
end
