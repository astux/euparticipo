class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    auth = request.env['omniauth.auth']

    @user = User.find_from_omniauth_uid(auth)

    if !@user.nil?
      @user.refresh_facebook_token(auth)
      sign_in_and_redirect @user
    elsif auth.extra.raw_info.email && @existing_user = User.where(email: auth.extra.raw_info.email).first
      @existing_user.provider = auth.provider if auth.provider
      @existing_user.uid = auth.uid if auth.uid
      @existing_user.facebook_token = auth['credentials']['token'] if auth['credentials']['token']
      @existing_user.remote_photo_url = @existing_user.profile_picture if @existing_user.photo.blank?
      @existing_user.save
      
      sign_in_and_redirect @existing_user
    else
      @user = User.new_user_from_omniauth(auth)
      @photo_url = @user.profile_picture
      render 'complete_registration'
    end
  end

  def failure
    #redirect_to URI.escape("http://staging-makemynight.herokuapp.com/facebook/error?error=#{params[:error]}&error_code=#{params[:error_code]}&error_description=#{params[:error_description]}&error_reason=#{params[:error_reason]}")
    #redirect_to controller: '/facebook', action: 'error', error: params[:error], error_code: params[:error_code], error_description: params[:error_description], error_reason: params[:error_reason]
    return false
  end
end