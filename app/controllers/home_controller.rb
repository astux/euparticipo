class HomeController < ApplicationController
  layout :layout_definition

  def index
    if user_signed_in?
      @user = current_user
      @page = params[:page] || 1
      @posts = @user.timeline.page @page
      
      respond_to do |format|
        format.html{ render 'index-logged' }
        format.js { render 'users/timeline' }
      end
    else
      @user = User.new
    end
  end

  def cadastro_parlamentar
    @user = User.new
  end

  def sobre
    render 'euparticipo/sobre'
  end

  private

  def layout_definition
    if user_signed_in?
      'application'
    else
      'home'
    end
  end
end
