class PreferencesController < ApplicationController
  before_filter :authenticate_user!, :load_user
  
  def index
  end
  
  def update
    if @user.update_attributes resource_params
      flash[:success] = 'Preferências de email atualizadas com sucesso.'
      redirect_to root_path
    else
      flash[:error] = 'Não foi possível atualizar suas preferências de email. Por favor, tente novamente.'
      render 'index'
    end
  end
  
  protected
  
  def load_user
    @user = current_user
  end
  
  def resource_params
    params.require(:user).permit(:mail_digest, :mail_nudge, :mail_newsletter)
  end
end
