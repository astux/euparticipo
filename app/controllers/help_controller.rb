class HelpController < ApplicationController
  before_filter :authenticate_user!, :set_user
  
  def how_it_works
    
  end
  
  def set_user
    @user = current_user
  end
end
