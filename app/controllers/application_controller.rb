# -*- coding: utf-8 -*-
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :check_user_verification
  before_filter :load_notifications

  unless Rails.application.config.consider_all_requests_local
    rescue_from Exception, with: :render_500
    rescue_from ActionController::RoutingError, with: :render_404
    rescue_from ActionController::UnknownController, with: :render_404
    rescue_from ActiveRecord::RecordNotFound, with: :render_404
  end

  def render_404(exception=nil)
    @not_found_path = exception.message if exception
    respond_to do |format|
      format.html { render template: 'errors/error_404', layout: 'clean', status: 404 }
      format.all { render nothing: true, status: 404 }
    end
  end

  def render_500(exception)
    @error = exception
    logger.error exception.backtrace.join("\n")

    respond_to do |format|
      format.html { render template: 'errors/error_500', layout: 'clean', status: 500 }
      format.all { render nothing: true, status: 500 }
    end
  end

  def routing_error
    respond_to do |format|
      format.html { render template: 'errors/routing_error', layout: 'clean' }
      format.all { render nothing: true, status: 500 }
    end
  end

  private
  
  def check_user_verification
    if user_signed_in? && !current_user.verified?
      sign_out current_user
      flash[:notice] = "Cadastrados de políticos devem ser confirmados pela administração, que ainda não confirmou esta conta."
      redirect_to '/'
    end
  end
  
  def load_notifications
    if !request.xhr? && user_signed_in?
      @notifications_count = current_user.notifications.since(current_user.notifications_read_date).count
    end
  end
end
