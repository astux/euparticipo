class FollowsController < ApplicationController
  respond_to :html
  before_filter :load_user
  
  def allies
    @users = Follow.allies(@user).page params[:page]
    
    respond_to do |format|
      format.html { @page_title = 'Aliados'; render 'listing' }
    end
  end
  
  def following
    @users = Follow.following_only(@user).page params[:page]
    
    respond_to do |format|
      format.html { @page_title = 'Acompanha'; render 'listing' }
    end
  end
  
  def followers
    @users = Follow.followers_only(@user).page params[:page]
    
    respond_to do |format|
      format.html { @page_title = 'Acompanham'; render 'listing' }
    end
  end
  
  def load_user
    @user = User.find_by_token! params[:user_id]
  end
end
