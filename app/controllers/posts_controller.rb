class PostsController < ApplicationController
  include PostsHelper
  respond_to :html, :js
  before_action :authenticate_user!, except: [:show, :likes]

  def create
    @post = Post.create(post_params)
    
    respond_to do |format|
      format.html { redirect_to request.referer }
      format.js
    end
  end
  
  def show
    @user = User.find_by_token! params[:user_id]
    @post = Post.find params[:id]
    
    @followers = @user.followers.limit(12)
    @followeds = @user.followeds.limit(12)
    @follows = (@followeds + @followers).uniq
    @follows_count = @follows.count
    @allied_politicians = @user.allied_politicians
  end

  def destroy
    @post = Post.find params[:id]
    if current_user.id == @post.user_id
      @post.destroy
    end
  end

  def like_toggle
    @post = Post.find params[:post_id]
    @text = ''
    if current_user.likes_post?(@post)
      current_user.dislikes_post(@post)
      @text = 'Curtir ' + @post.likes.count.to_s
    else
      current_user.likes_post(@post)
      @text = 'Curtir (Remover) ' + @post.likes.count.to_s
    end
    render layout: false
  end
  
  def likes
    @post = Post.find params[:id]
    @likes = Like.where(post: @post).limit(100)
  end
  
  def status
    @post = Post.find_by_id! params[:id]
    if !params[:set].blank? && valid_posts_status.include?(params[:set]) && @post.participe_user_id == current_user.id
      @status = params[:set]
      
      @post.status = @status
      @post.save
    end
    
    respond_to do |format|
      format.js
      format.html { redirect_to user_post_path(@post.user, @post) }
    end
  end

  def post_params
    params.require(:post).permit(:content,
                                 :user_id,
                                 :participe_user_id,
                                 :post_type, 
                                 :picture)
  end
end
