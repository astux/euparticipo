class CitiesController < ApplicationController
  def index
    @cities = City.where(state_id: params[:state_id]).order('name ASC')
    render :json => @cities
  end

  def show
    @city = City.where(state_id: params[:state_id], id: params[:id]).order('name ASC').first
    render :json => @city
  end
end
