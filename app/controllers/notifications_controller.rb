class NotificationsController < ApplicationController
  def index
    @user = current_user
    @last_read = current_user.notifications_read_date || DateTime.now - 100.years
    
    current_user.notifications_read_date = DateTime.now
    current_user.save
    
    @notifications = current_user.notifications.order('updated_at DESC').limit(50)
    load_notifications
  end
end
