class PhotosController < ApplicationController
  respond_to :html, :js
  before_filter :set_user
  
  def index
    @posts = @user.posts.order('created_at DESC').where('picture IS NOT NULL').page(params[:page]).per(15)
  end
  
  def show
    @post = Post.find params[:id]
    
    @previous = @user.posts.photos.where('created_at >= ? AND id <> ?', @post.created_at, @post.id).order('created_at ASC').first
    @next = @user.posts.photos.where('created_at <= ? AND id <> ?', @post.created_at, @post.id).order('created_at DESC').first
  end
  
  def set_user
    @user = User.find_by_token! params[:user_id]
  end
end
