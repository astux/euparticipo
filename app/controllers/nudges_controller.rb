class NudgesController < ApplicationController
  respond_to :html
  before_filter :authenticate_user!
  
  def create
    @from = current_user
    @to = User.find_by_id! resource_params[:to_id]
    
    Nudge.transaction do
      @nudge = Nudge.new resource_params
      @nudge.from = @from
      if @nudge.save
        InviteMailer.nudge(@nudge).deliver
        flash[:success] = "Pedido de atenção a #{@to.display_name} enviado com sucesso."
      else
        flash[:error] = "Você não pode mais chamar a atenção de #{@to.display_name}."
      end
    end
    
    redirect_to user_path(@to)
  end
  
  def resource_params
    params.require(:nudge).permit(:to_id)
  end
end
