class PollsController < ApplicationController
  def vote
    @poll = Poll.find params[:id]
    @option = params[:option]
    @vote = PollVote.register_vote(@poll, current_user, @option)
  end
end
