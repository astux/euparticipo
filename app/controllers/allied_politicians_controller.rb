class AlliedPoliticiansController < ApplicationController
  before_filter :authenticate_user!, :set_user
  
  def index
    @position = params[:position]
    @position_name = case @position
    when 'vereador' then 'Vereador'
    when 'deputado-estadual' then 'Deputado Estadual'
    when 'deputado-federal' then 'Deputado Federal'
    when 'senador' then 'Senador'
    end
    
    @users = User.ordered.page(params[:page])
    @users.where!(political_position: @position_name, verified: "t")
    
    @uf = params[:uf] ? params[:uf] : nil 
    if !@uf.blank?
      @state = State.find_by_abbreviation @uf
      @users.where!(political_state_id: @state.id.to_s)
    end
    
    @cidade_id = params[:cidade] ? params[:cidade] : nil
    if !@cidade_id.blank?
      @city = City.find_by_id @cidade_id
      @users.where!(political_city_id: @city.id.to_s)
    end
  end
  
  def ally
    @politician = User.find_by_token! params[:id]
    @previous_alliance = Alliance.where(citizen_id: current_user.id).where_position(@politician.political_position).first
    @alliance = Alliance.ally(current_user, @politician)
    
    @allied = true
  end
  
  def unally
    @politician = User.find_by_token! params[:id]
    @alliance = Alliance.where(citizen_id: current_user.id, politician_id: @politician.id).first
    @alliance.destroy if @alliance
    
    @allied = false
    
    render 'ally'
  end
  
  def recommend
    @user = User.find_by_token! params[:user_id]
    @politician = User.find_by_token! params[:id]
    
    @suggestion = AllianceSuggestion::suggest(current_user, @user, @politician)
    
    respond_to do |format|
      format.js
      format.html { 
        flash[:success] = "Indicação de #{@politician.display_name} feita com sucesso."
        redirect_to user_path(@user)
      }
    end
  end
  
  def set_user
    @user = current_user
  end
end
