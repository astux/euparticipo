class Dev::MailingController < ApplicationController
  layout 'email'
  
  def confirmacao_cadastro
    @resource = User.first
    
    render 'devise/mailer/confirmation_instructions'
  end
  
  def digest
    @user = User.find params[:user_id]
    @notifications = WeeklyDigest.notifications_for(@user)
    
    render 'digest_mailer/digest'
  end
end
