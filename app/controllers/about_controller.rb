class AboutController < ApplicationController
  layout 'clean'

  def about
  end

  def privacy
  end

  def terms
  end

  def send_contact
    ContactMailer.user_contact(params[:name], params[:email], params[:subject], params[:message]).deliver
    flash[:notice] = "Mensagem enviada com sucesso. Obrigado pelo seu contato."
    redirect_to params[:origin]
  end
end
