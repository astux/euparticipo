# -*- coding: utf-8 -*-
class UsersController < ApplicationController
  before_action :set_user, except: [:search]

  def show
    @page = params[:page] || 1
    @posts = @user.posts.order('id DESC').page @page
    
    respond_to do |format|
      format.html
      format.js { render 'timeline' }
    end
  end

  def edit
    # problema ao tentar trocar token para um token que jah existe?
    # apenas permite editar o usuário corrente logado
    if user_signed_in? and current_user.id == @user.id
      @editable_user = current_user
    else
      redirect_to root_path
    end
  end

  def update
    @editable_user = current_user
    if @editable_user.update(post_params)
      flash[:success] = 'Usuário editado.'
      redirect_to root_path
    else
      render action: 'edit'
    end
  end

  def edit_password
    @user = current_user
  end

  def update_password
    @user = User.find(current_user.id)

    if @user.update_with_password(params.required(:user).permit(:password, :password_confirmation, :current_password))
      # Sign in the user by passing validation in case his password changed
      sign_in @user, :bypass => true
      flash[:success] = 'Sua senha foi alterada.'
      redirect_to root_path
    else
      flash[:error] = 'Sua senha não foi alterada.'
      render "edit_password"
    end
  end

  def follow
    @user = User.find_by_token params[:id]
    unless current_user.follow(@user)
      flash[:error] = 'Você já está aliada(o) um(a) parlamentar desta categoria. Você só pode aliar-se a um(a) de cada categoria.'
    end
  end

  def unfollow
    @user = User.find_by_token params[:id]
    current_user.unfollow(@user)
    render 'follow'
  end

  def participa
    participa_types = Post.participa_post_types
    @page = params[:page] || 1
    @posts = Post.where('user_id = ? AND post_type IN (?)', @user.id, participa_types).order('updated_at DESC').page @page
    
    respond_to do |format|
      format.html
      format.js { render 'timeline' }
    end
  end
  
  # participe ================================
  def participe
    @posts = Post.where(participe_user_id: @user.id, post_type: Post.participa_post_types).order('updated_at DESC').page params[:page]
  end
  
  def participe_subpagina
    load_post_type_subpagina
    @page = params[:page] ? params[:page].to_i : 1
    @posts = Post.where(participe_user_id: @user.id, post_type: @post_type).order('updated_at DESC').page params[:page]
    
    respond_to do |format|
      format.html
      format.js { render 'timeline' }
    end
  end

  def load_post_type_subpagina
    @post_type = case params[:subpagina]
    when 'lei' then 'Lei'
    when 'discurso' then 'Discurso'
    when 'voto' then 'Voto'
    when 'denuncia' then 'Denúncia'
    when 'requerimento' then 'Requerimento'
    when 'audiencia' then 'Audiência Pública'
    when 'agraciamento' then 'Título e Agraciamento'
    when 'emenda-orcamentaria' then 'Emenda Orçamentária'
    end
  end
  
  # nossa atuação ================================

  def nossa_atuacao
    @page = params[:page] ? params[:page].to_i : 1
    @posts = Post.where(participe_user_id: @user.id, user_id: @user.id, post_type: Post.nossa_atuacao_post_types).order('updated_at DESC').page params[:page]
    
    respond_to do |format|
      format.html
      format.js { render 'timeline' }
    end
  end
  
  def nossa_atuacao_subpagina
    load_post_type_subpagina_nossa_atuacao
    @page = params[:page] ? params[:page].to_i : 1
    @posts = Post.where(participe_user_id: @user.id, user_id: @user.id, post_type: @post_type).order('updated_at DESC').page params[:page]
    
    respond_to do |format|
      format.html
      format.js { render 'timeline' }
    end
  end

  def load_post_type_subpagina_nossa_atuacao
    @post_type = case params[:subpagina]
    when 'em-acao' then 'Em Ação'
    when 'proposicoes' then 'Proposições'
    when 'artigos-e-publicacoes' then 'Artigos e Publicações'
    when 'na-midia' then 'Na Mídia'
    when 'agenda' then 'Agenda'
    when 'prestacao-de-contas' then 'Prestação de Contas'
    end
  end

########### /nossa atuacao
  def biography
    redirect_to root_path unless @user.is_politician?
  end
  
  def update_biography
    if user_signed_in? && @user == current_user
      current_user.biography = params[:user][:biography]
      if current_user.save
        flash[:success] = 'Biografia atualizada com sucesso.'
        redirect_to biografia_user_path(current_user)
      end
    end
  end

  def dados_pessoais
    #redirect_to root_path unless @user.is_politician?
  end

  def contatos
    redirect_to root_path unless @user.is_politician?
  end

  def search
    if user_signed_in?
      @user = current_user
    end
    
    params[:q] ||= ''
    
    param_formatted = '%' + params[:q].split.join('%') + '%'
    query = 'verified IS TRUE AND ( users.name || users.last_names ilike ? OR users.political_nickname ilike ? )'
    @users = User.where(query, param_formatted, param_formatted).ordered.page(params[:page])
    
    if @user && params[:method] && params[:method] == 'discover'
      following_ids = @user.follows_followeds.map(&:followed_id) + [@user.id]
      @users.where!('users.id NOT IN (?)', following_ids)
    end
    
    @uf = nil
    if !params[:uf].blank?
      @state = State.find_by_abbreviation params[:uf]
      @uf = @state.abbreviation
      @users.where!(state_id: @state.id)
    end
    
    @cidade_id = nil
    if !params[:cidade].blank?
      @city = City.find_by_id params[:cidade]
      @cidade_id = @city.id
      @users.where!(city_id: @city.id)
    end
  end
  
  def o_que_faz_um
  end

  private 

  def set_user
    @user = User.find_by_token! params[:id]
  end

  def post_params
    params.require(:user).permit(:name,
                                 :birthday,
                                 :gender,
                                 :email,
                                 :verified,
                                 :password,
                                 :password_confirmation,
                                 :last_names,
                                 :work_info,
                                 :scholarship,
                                 :relationship_status,
                                 :political_preference,
                                 :about_me,
                                 :favorite_quote,
                                 :biography,
                                 :verified,
                                 :role,
                                 :political_position,
                                 :political_nickname,
                                 :state_id,
                                 :city_id,
                                 :from_state_id,
                                 :from_city_id,
                                 :political_state_id,
                                 :political_city_id,
                                 :photo,
                                 :token,
                                 :party_id,
                                 :confirmed_at,
                                 :phone1,
                                 :phone2, 
                                 :show_birthday)
  end
end

