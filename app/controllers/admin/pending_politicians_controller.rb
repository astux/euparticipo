class Admin::PendingPoliticiansController < Admin::BaseController
  before_filter :load_pending_politician
  
  def show
  end
  
  def approve
    @user.verified = true
    @user.save
    
    flash[:notice] = "#{@user.political_nickname} aprovado."
    AdminMailer.approve_politician(@user).deliver
    redirect_to admin_dashboard_index_path
  end
  
  def deny
    @name = @user.political_nickname
    @email = @user.email
    @user.destroy
    
    flash[:notice] = "#{@name} recusado."
    AdminMailer.deny_politician(@name, @email).deliver
    redirect_to admin_dashboard_index_path
  end
  
  def load_pending_politician
    @user = User.find_by_id params[:id]
  end
  protected :load_pending_politician
end
