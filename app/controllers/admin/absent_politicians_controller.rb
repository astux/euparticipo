class Admin::AbsentPoliticiansController < Admin::BaseController
  respond_to :html, :js
  before_filter :load_absent_politician, except: [:index, :create, :new]
  
  def index
    @politicians = AbsentPolitician.order('name ASC').page params[:page]
    @absent_politician = AbsentPolitician.new
  end
  
  def create
    @absent_politician = AbsentPolitician.new resource_params
    @absent_politician.save
    
    respond_with @absent_politician, location: admin_absent_politicians_path
  end
  
  def update
    @absent_politician.update_attributes resource_params
    respond_with @absent_politician, location: admin_absent_politicians_path
  end
  
  def destroy
    @absent_politician.destroy
    respond_with @absent_politician, location: admin_absent_politicians_path
  end
  
  def load_absent_politician
    @absent_politician = AbsentPolitician.find params[:id]
  end
  
  def resource_params
    params.require(:absent_politician).permit!
  end
end