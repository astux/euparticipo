class Admin::DashboardController < Admin::BaseController
  def index
    @pending = User.where(role: 'Legislativo').where('verified IS NOT TRUE').page params[:page]
  end
end
