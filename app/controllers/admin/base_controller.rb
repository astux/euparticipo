class Admin::BaseController < ApplicationController
  before_filter :authenticate_user!, :check_permission
  
  def check_permission
    if Rails.env.production?
      if current_user.email != 'marcelo_bcastro@yahoo.com.br' and
          current_user.email != 'marcelo@euparticipo.com.br' and
          current_user.email != 'gilmagno@gmail.com'
        redirect_to root_path
        return false
      end
    end
  end
end
