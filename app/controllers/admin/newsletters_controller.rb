class Admin::NewslettersController < Admin::BaseController
  respond_to :html
  
  def index
    @newsletters = Newsletter.ordered.page params[:page]
  end
  
  def new
    @newsletter = Newsletter.new
  end
  
  def create
    @newsletter = Newsletter.new resource_params
    
    if params[:commit] && params[:commit] == 'Salvar'
      flash[:notice] = 'Newsletter salva com sucesso.' if @newsletter.save
    elsif params[:commit] && params[:commit] == 'Salvar e Enviar'
      begin
        Newsletter.transaction do
          @newsletter.publish
          
          User.where(mail_newsletter: true).each do |u|
            NewsletterMailer.newsletter(u, @newsletter).deliver
          end
          
          flash[:success] = 'Newsletter publicada com sucesso. Ela será enviada nos próximos minutos.'
        end
      rescue
        @newsletter.save
        flash[:error] = 'Não foi possível enviar a newsletter.'
      end
    end
      
    respond_with @newsletter, location: admin_newsletters_path
  end
  
  def edit
    @newsletter = Newsletter.find params[:id]
  end
  
  def destroy
    @newsletter = Newsletter.find params[:id]
    if @newsletter.destroy
      flash[:success] = 'Newsletter excluída com sucesso.'
    end
    
    respond_with @newsletter, location: admin_newsletters_path
  end
  
  def resource_params
    params.require(:newsletter).permit(:subject, :content)
  end
end
