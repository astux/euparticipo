class UserVotesController < ApplicationController
  respond_to :html, :js
  
  before_filter :load_user
  before_filter :authenticate_user!, only: [:create, :destroy]
  
  def index
    @votes = @user.votes_received.includes(:user).page params[:page]
    @my_vote = UserVote.where(user_id: @user.id).first
  end
  
  def create
    @vote = UserVote.register_vote(current_user, @user)
    
    respond_with @vote, location: user_votes_path(@user)
  end
  
  def destroy
    UserVote.where(user_id: current_user.id, voted_user_id: @user.id).delete_all
    
    respond_to do |format|
      format.html {redirect_to user_votes_path(@user)}
      format.js { render 'create' }
    end
  end
  
  def load_user
    @user = User.find_by_token! params[:user_id]
  end
  protected :load_user
end
