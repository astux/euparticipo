class CommentsController < ApplicationController
  def index
    @post = Post.find params[:post_id]
    @comments = @post.comments.page params[:page]
  end
  
  def create
    @user = User.find_by_token params[:user_id]
    params[:comment][:user_id] = @user.id
    params[:comment][:post_id] = params[:post_id]
    @comment = Comment.create(comment_params)
    @post = @comment.post
  end

  def destroy
    @comment = Comment.find params[:id]
    @post = @comment.post
    @comment.destroy
  end

  def comment_params
    params.require(:comment).permit(:content,
                                    :user_id,
                                    :post_id)
  end
end
