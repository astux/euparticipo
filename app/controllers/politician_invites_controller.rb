class PoliticianInvitesController < ApplicationController
  respond_to :html
  
  before_filter :authenticate_user!
  before_filter :load_user
  
  def index
    
  end
  
  def search
    @results = AbsentPolitician.order('name ASC').page params[:page]
    @results.where!('name ILIKE ?', "%#{params[:name]}%") if !params[:name].blank?
    @results.where!(political_position: params[:political_position]) if !params[:political_position].blank?
    
    respond_to do |format|
      format.html { render 'index' }
      format.js
    end
  end
  
  def invite
    @success = false
    #begin
      @politician = AbsentPolitician.find params[:id]
      
      if @politician
        PoliticianInvite.transaction do
          @invite = PoliticianInvite.new
          @invite.absent_politician_id = @politician.id
          @invite.user_id = current_user.id
          if @invite.save
            InviteMailer.politician(@invite).deliver
            @success = true
          end
        end
      end
    #rescue
    #end
    
    respond_to do |format|
      format.html {
        flash[:success] = 'Político convidado com sucesso.' if @success
        redirect_to politician_invites_path
      }
      format.js
    end
  end
  
  def create
    PoliticianInvite.transaction do
      @invite = PoliticianInvite.new resource_params
      @invite.user_id = current_user.id
      if @invite.save
        InviteMailer.politician(@invite).deliver
        flash[:success] = "Seu convite a #{@invite.absent_politician.name} foi enviado com sucesso. Muito obrigado!"
      end
    end
    
    respond_with @invite, location: politician_invites_path
  end
  
  def load_user
    @user = current_user
    redirect_to root_path if @user.is_politician?
  end
  
  def resource_params
    params.require(:politician_invite).permit(:absent_politician_id)
  end
end
