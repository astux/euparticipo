class Comment < ActiveRecord::Base
  belongs_to :post
  belongs_to :user
  
  include Notifiable
  
  def notification_pack
    if self.post.user_id != self.user_id
      existent = UserNotification.where(action_id: UserNotification::COMMENT, entity_id: self.post_id)
                                 .where('created_at >= ?', DateTime.now - 1.day)
                                 .first
      
      if existent
        existent.update_attributes(origin_ids: [self.user_id] | existent.origin_ids)
        []
      else
        [
          {
            recipients: [self.post.user_id],
            entity_id: self.post_id,
            origin_ids: [self.user_id],
            action_id: UserNotification::COMMENT
          }
        ]
      end
    end
  end
end
