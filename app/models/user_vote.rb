class UserVote < ActiveRecord::Base
  validates :user_id, uniqueness: true
  belongs_to :user
  belongs_to :voted_user, class_name: "User"
  
  scope :to_user, lambda { |u| self.where(voted_user_id: u.id) }
  
  def self.register_vote(voter, voted)
    transaction do
      scope = self.where(user_id: voter.id, voted_user_id: voted.id)
      self.where(user_id: voter.id).delete_all
      scope.create
    end
  end
  
  include Notifiable
  
  def notification_pack
    [
      {
        recipients: [self.voted_user_id],
        entity_id: self.id,
        origin_ids: [self.user_id],
        action_id: UserNotification::VOTE
      }
    ]
  end
end
