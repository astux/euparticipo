class PoliticianInvite < ActiveRecord::Base
  belongs_to :absent_politician
  belongs_to :user
  
  validates :absent_politician, :user, presence: true
  validate :invite_period
  
  def invite_period
    if !PoliticianInvite.can_invite?(user_id, absent_politician_id)
      errors.add(:absent_politician_id, "#{absent_politician.name} já foi convidado por você uma vez.")
    end
  end
  
  def self.can_invite?(user_id, absent_politician_id)
    !PoliticianInvite.where(user_id: user_id, absent_politician_id: absent_politician_id).exists?
  end
end
