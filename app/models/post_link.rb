class PostLink < ActiveRecord::Base
  has_many :posts
  
  mount_uploader :image, LinkThumbnailUploader
  
  validates :url, :title, presence: true
end
