class WeeklyDigest
  def self.digestables
    User.where(mail_digest: true).where('last_digest_sent_at IS NULL OR last_digest_sent_at >= ?', DateTime.now - 7.days)
  end
  
  def self.notifications_for(user)
    notifications = user.notifications.order('updated_at DESC').limit(20)
    notifications.where!('updated_at > ?', user.last_digest_sent_at) if !user.last_digest_sent_at.blank?
    
    notifications
  end
  
  def self.register_sent_digest(user)
    user.last_digest_sent_at = DateTime.now
    user.save
  end
end