class Nudge < ActiveRecord::Base
  THRESHOLD=15
  
  belongs_to :from, class_name: 'User'
  belongs_to :to, class_name: 'User'
  
  validates :from, :to, presence: true
  
  def self.can_nudge?(from, to)
    existing = Nudge.where(from_id: from.id, to_id: to.id).first
    
    !existing && (to.last_sign_in_at.blank? || Date.today - to.last_sign_in_at.to_date >= Nudge::THRESHOLD)
  end
end
