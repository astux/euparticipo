# -*- coding: utf-8 -*-
class Post < ActiveRecord::Base
  STATUS_DEBATE='em-debate'
  STATUS_NAO_ADOTADA='nao-adotada'
  STATUS_ADOTADA='adotada'
  STATUS_APROVADA='aprovada'
  STATUS_REJEITADA='rejeitada'
  
  mount_uploader :picture, PictureUploader
  
  before_save :create_link, on: :create
  after_create :create_poll, :if => :is_poll?
  
  belongs_to :user, class_name: 'User', foreign_key: 'user_id'
  belongs_to :participe_user, class_name: 'User', foreign_key: 'participe_user_id'
  belongs_to :post_link
  
  has_many :comments, dependent: :delete_all
  has_many :likes, dependent: :delete_all
  has_many :polls, dependent: :destroy
  
  scope :photos, -> { where('picture IS NOT NULL') }
  
  paginates_per 10

  def is_poll?
    self.post_type == 'Voto'
  end

  def self.participa_post_types
    return ['Lei', 'Discurso', 'Voto', 'Denúncia', 'Requerimento', 'Audiência Pública', 'Título e Agraciamento', 'Emenda Orçamentária']
  end

  def self.nossa_atuacao_post_types
    return ['Em Ação', 'Na Mídia', 'Artigos e Publicações', 'Agenda', 'Proposições', 'Prestação de Contas']
  end

  def participa?
    return true if Post.participa_post_types.include?(self.post_type)
    return false
  end

  def nossa_atuacao?
    return true if Post.nossa_atuacao_post_types.include?(self.post_type)
    return false
  end
  
  def create_poll
    Poll.create(post_id: self.id)
  end
  
  def can_interact?(user)
    if self.participa?
      return user.id == self.participe_user_id || user.allied?(self.participe_user) || (user.is_politician? && self.user_id == user.id)
    elsif self.nossa_atuacao?
      return user.id == self.participe_user_id || user.follows?(self.participe_user) || (user.is_politician? && self.user_id == user.id)
    else
      return true
    end
  end
  
  def create_link
    # Inserting http before text
    content.gsub!(/www\./, "http://www.")
    content.gsub!(/http:\/\/http:\/\/www\./, "http://www.")
    content.gsub!(/https:\/\/http:\/\/www\./, "https://www.")
    
    matches = content.match(/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/)
    if matches && matches[0] && match = matches[0]
      if ((match =~ /^http(s)?:\/\/(www\.)?youtube\.com\//) == nil) && ((match =~ /^http(s)?:\/\/(www\.)?youtu\.be\//) == nil)
        link = PostLink.find_by_url match
        
        if !link || ((Date.today.mjd - link.updated_at.to_date.mjd) > 30)
          begin
            thumbnailer = LinkThumbnailer.generate(match)
            
            if thumbnailer
              link = PostLink.new if !link
              
              link.url = thumbnailer.url
              link.title = thumbnailer.title
              link.description = thumbnailer.description if thumbnailer.description?
              if thumbnailer.image?
                link.remote_image_url = thumbnailer.image
              elsif thumbnailer.images && thumbnailer.images.size > 0
                link.remote_image_url = thumbnailer.images.first.source_url.to_s
              end
              
              if !link.id.blank?
                link.save
              end
              
              self.post_link = link
            end
          rescue
          end
        else
          self.post_link_id = link.id
        end
      end
    end
  end
end
