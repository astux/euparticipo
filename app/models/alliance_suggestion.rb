class AllianceSuggestion
  def self.suggest(from, to, politician)
    existent = UserNotification.where(user_id: to.id, 
                                      entity_id: politician.id, 
                                      action_id: UserNotification::ALLIANCE_SUGGESTION)
                               .where("? = ANY(origin_ids)", from.id)
                               .where('created_at >= ?', DateTime.now - 1.day)
                               .first
    
    if !existent
      UserNotification.create(user_id: to.id, 
                              origin_ids: [from.id], 
                              entity_id: politician.id, 
                              action_id: UserNotification::ALLIANCE_SUGGESTION)
    else
      existent
    end
  end
end