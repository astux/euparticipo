class Follow < ActiveRecord::Base
  MINIMUM_REQUIRED = 20
  
  belongs_to :follower, { :class_name => 'User', :foreign_key => 'follower_id' }
  belongs_to :followed, { :class_name => 'User', :foreign_key => 'followed_id' }
  
  include Notifiable
  
  def notification_pack
    [
      {
        recipients: [self.followed_id],
        entity_id: self.follower_id,
        origin_ids: [self.follower_id],
        action_id: UserNotification::FOLLOW
      }
    ]
  end
  
  def self.following_only(user)
    users = user.followeds.ordered
    if user.is_politician?
      users = users.unallied_to(user)
    else
      users = users.no_mutual_follows_with(user)
    end
    
    users
  end
  
  def self.followers_only(user)
    users = user.followers.ordered
    if user.is_politician?
      users = users.unallied_to(user)
    else
      users = users.no_mutual_follows_with(user)
    end
    
    users
  end
  
  def self.allies(user)
    if user.is_politician?
      users = user.allied_citizens
    else
      users = User.mutual_follows_with(user)
    end
    
    users = users.ordered
  end
end
