class AbsentPolitician < ActiveRecord::Base
  belongs_to :city
  validates :name, :email, :political_position, presence: true
  validates :email, uniqueness: true
  
  mount_uploader :photo, PhotoUploader
  
  def full_name
    "#{name} (#{political_position}) #{' - ' + city.name if city}"
  end
end
