class UserNotification < ActiveRecord::Base
  FOLLOW=1
  LIKE=2
  COMMENT=3
  VOTE=4
  ALLIANCE_SUGGESTION=5
  
  scope :to, lambda { |u| where(user_id: u.id) }
  scope :since, lambda { |dt| where('updated_at >= ?', dt || DateTime.now - 100.years) }
  
  belongs_to :user
end
