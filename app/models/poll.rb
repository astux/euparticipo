class Poll < ActiveRecord::Base
  belongs_to :post
  has_many :poll_votes, dependent: :delete_all
end
