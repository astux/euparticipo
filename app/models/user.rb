# -*- coding: utf-8 -*-
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable, 
         :omniauthable, :omniauth_providers => [:facebook]

  validates :name, presence: true, :unless => lambda { self.is_politician? }
  validates :last_names, presence: true, :unless => lambda { self.is_politician? }
  validates :political_nickname, presence: true, :if => lambda { self.is_politician? }
  validates :photo, presence: true, :if => lambda { self.is_politician? }
  validates :email, presence: true, uniqueness: true
  validates :token, presence: true, uniqueness: true
  validates :political_state_id, presence: true, :if => :is_politician?
  validates :political_city_id, presence: true, :if => :needs_city?
  validates :party_id, presence: true, :if => :is_politician?
  validates :political_position, presence: true, :if => :is_politician?

  validates :about_me, length: {maximum: 200} 
  validates :favorite_quote, length: {maximum: 200}

  validates :name, length: {maximum: 13}
  validates :last_names, length: {maximum: 13}

  validates :work_info, length: {maximum: 45}
  validates :scholarship, length: {maximum: 200}

  validates :phone1, presence: true, :if => lambda { self.is_politician? }

  belongs_to :party

  belongs_to :city, class_name: 'City', foreign_key: 'city_id'
  belongs_to :state, class_name: 'State', foreign_key: 'state_id'

  belongs_to :political_city, class_name: 'City', foreign_key: 'political_city_id'
  belongs_to :political_state, class_name: 'State', foreign_key: 'political_state_id'

  belongs_to :from_city, class_name: 'City', foreign_key: 'from_city_id'
  belongs_to :from_state, class_name: 'State', foreign_key: 'from_state_id'
  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :delete_all
  has_many :likes, dependent: :delete_all

  # Follow
  has_many :follows_followers, :class_name => 'Follow', :foreign_key => 'followed_id'
  has_many :follows_followeds,  :class_name => 'Follow', :foreign_key => 'follower_id' 
  has_many :followers, :through => :follows_followers, dependent: :delete_all
  has_many :followeds, :through => :follows_followeds, dependent: :delete_all

  # Alliance
  has_many :alliances_politicians, :class_name => 'Alliance', :foreign_key => 'citizen_id'
  has_many :alliances_citizens, :class_name => 'Alliance', :foreign_key => 'politician_id'
  has_many :allied_politicians, :through => :alliances_politicians, dependent: :delete_all
  has_many :allied_citizens, :through => :alliances_citizens, dependent: :delete_all
  
  has_many :notifications, :class_name => 'UserNotification', dependent: :delete_all
  
  has_many :user_votes, dependent: :delete_all
  has_many :votes_received, class_name: 'UserVote', foreign_key: 'voted_user_id', dependent: :delete_all
  
  mount_uploader :photo, PhotoUploader

  scope :ordered, lambda { select('users.*, coalesce(users.political_nickname, users.name) as dname').order('dname ASC, last_names ASC') }
  scope :unallied_to, lambda { |u| where('users.id NOT IN (SELECT citizen_id FROM alliances a WHERE a.citizen_id = users.id AND a.politician_id = ?)', u.id) }
  scope :mutual_follows_with, lambda { |u| where('users.id IN (SELECT follower_id FROM follows f WHERE f.followed_id = ?)', u.id)
                                           .where('users.id IN (SELECT followed_id FROM follows f WHERE f.follower_id = ?)', u.id) } 
  scope :no_mutual_follows_with, lambda { |u| ids = User.mutual_follows_with(u).select('users.id'); 
                                              where('users.id NOT IN (?)', ids.size > 0 ? ids : '-1') }

  before_validation :make_token

  def needs_city?
    self.political_position == 'Vereador'
  end

  def make_token
    name = ''

    if self.political_nickname
      name = self.political_nickname
    elsif self.name
      name = self.name
    end

    token = name.parameterize

    user = User.where(token: token).first

    if user and user.id == self.id
      self.token = token
      return
    end

    users = User.where('token like ?', token + '%')

    if users.length > 0
      tokens = []

      users.each do |u|
        tokens.push u.token
      end

      token_root = token
      suffix = 1

      while tokens.include?(token)
        suffix = suffix.to_i + 1
        token = token_root + suffix.to_s
      end
    end

    self.token = token
  end

  def to_param
    return token
  end

  def is_politician?
    if self.role == 'Legislativo' or self.role == 'Judiciário' or self.role == 'Executivo' or self.role == 'Notório'
      return true
    end
    return false
  end

  def get_photo
    if self.photo.url
      return self.photo.url
    else
      return '/without_photo.jpg'
    end
  end

  def get_photo_thumb
    if self.photo.thumb.url
      return self.photo.thumb.url
    else
      return '/without_photo_thumb.jpg'
    end
  end

  # Retorna string que será concatenação do self.name com última parte do
  # self.last_names.
  def short_name
    if self.is_politician?
      return self.political_nickname
    end

    short_name = ''

    if self.name
      short_name += self.name.to_s.split(' ').first + ' '
    end

    if self.last_names
      short_name += self.last_names.to_s.split(' ').last
    end

    return short_name
  end

  def full_name
    self.name + ' ' + self.last_names
  end

  def first_name
    if self.is_politician?
      return self.political_nickname.to_s.split(' ').first
    else
      return self.name.to_s.split(' ').first
    end
  end

  def allied?(politician)
    Alliance.where(citizen_id: self.id, politician_id: politician.id).exists?
  end

  def follows?(user)
    follow = Follow.where(follower_id: self.id, followed_id: user.id).first
    if follow.nil?
      return false
    else
      return true
    end
  end

  def follow(user)
    Follow.create(follower_id: self.id, followed_id: user.id)
  end

  def unfollow(user)
    follow = Follow.where(follower_id: self.id, followed_id: user.id).first
    follow.destroy
  end

  def timeline
    ids = []
    ids.push self.id

    followeds = self.followeds
    if !followeds.empty?
      ids.concat followeds.map{ |f| f.id }
    end
    
    @posts = Post.order('updated_at DESC')
    
    if ids.size > Follow::MINIMUM_REQUIRED
      @posts.where!('user_id IN (' + ids.join(',') + ')')
    end
    
    @posts
  end

  def allied_politicians
    politicians = []
    alliances = Alliance.where(citizen_id: self.id).includes(:allied_politician).all 
    return alliances ? alliances.map(&:allied_politician) : []
  end

  def political_position_gendered
    if self.gender == 'Feminino'
        return 'Vereadora' if self.political_position == 'Vereador'
        return 'Deputada Estadual' if self.political_position == 'Deputado Estadual'
        return 'Duputada Federal' if self.political_position == 'Deputado Federal'
        return 'Senadora' if self.political_position == 'Senador'
    end
    return self.political_position    
  end

  def likes_post?(post)    
    like = Like.where('user_id = ? AND post_id = ?', self.id, post.id)
    if like.length == 0
      return false
    else
      return true
    end
  end

  def likes_post(post)
    like = Like.where(user_id: self.id, post_id: post.id)
    if like.length == 0
      Like.create(user_id: self.id, post_id: post.id)
    end
  end

  def dislikes_post(post)
    like = Like.where(user_id: self.id, post_id: post.id)
    like.each do |l| l.destroy end
  end
  
  def support_count
    Like.joins(:post).where(posts: {user_id: self.id, post_type: Post.participa_post_types}).count
  end
  
  def socrates_count
    self.posts.where(post_type: Post.participa_post_types).count
  end
  
  def plato_count
    self.support_count
  end
  
  # Omniauth
  def self.find_from_omniauth_uid(auth)
    User.where(provider: auth.provider, uid: auth.uid).first
  end

  def refresh_facebook_token(auth)
    self.facebook_token = auth['credentials']['token']
    self.save!
  end

  def self.new_user_from_omniauth(auth)
    data = auth.extra.raw_info

    user = User.new
    if data.name
      user.name = data.name.gsub(/\s.*/, '').strip()
      user.last_names = data.name.gsub(/.*\s/, '').strip()
    end
    user.email = data.email if data.email
    if data.gender
      if data.gender == 'male'
        user.gender = 'Masculino'
      elsif data.gender == 'female'
        user.gender = 'Feminino'
      end
    end
    user.birthday = Date.strptime(data.birthday, "%m/%d/%Y") if data.birthday
    user.provider = auth.provider if auth.provider
    user.uid = auth.uid if auth.uid
    user.facebook_token = auth['credentials']['token'] if auth['credentials']['token']
    
    return user
  end

  # Facebook Only
  def profile_picture
    "http://graph.facebook.com/#{uid}/picture?width=180&height=180"
  end
  
  def display_name
    is_politician? ? political_nickname : "#{name} #{last_names}"
  end
  
  protected
  
  def confirmation_required?
    uid.blank? && !confirmed?
  end
end
