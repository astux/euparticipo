class PollVote < ActiveRecord::Base
  belongs_to :poll
  belongs_to :user
  
  scope :vote_yes, lambda { where(yes: true) }
  scope :vote_no, lambda { where(no: true) }
  scope :vote_absent, lambda { where(absent: true) }
  
  def self.register_vote(d_poll, voter, option)
    if vote = self.where(user_id: voter.id, poll_id: d_poll.id).first
      [:yes, :no, :absent].each { |k| vote[k] = false }
    else
      vote = self.new(user_id: voter.id, poll_id: d_poll.id)
    end
    
    vote[option.to_sym] = true
    vote.save
  end 
end
