class Alliance < ActiveRecord::Base
  belongs_to :allied_citizen, { :class_name => 'User', :foreign_key => 'citizen_id' }
  belongs_to :allied_politician, { :class_name => 'User', :foreign_key => 'politician_id' }
  
  scope :where_position, lambda { |p| joins(:allied_politician).where(users: {political_position: p}).readonly(false) }
  
  after_destroy :delete_votes
  
  def self.ally(citizen, politician)
    transaction do
      row = Alliance.where(citizen_id: citizen.id).where_position(politician.political_position).first
      row = Alliance.new(citizen_id: citizen.id) if !row
      row.politician_id = politician.id
      row.save
      
      Follow.where(follower_id: citizen.id, followed_id: politician.id).first_or_create
    end
  end
  
  def delete_votes
    PollVote.where(user_id: citizen_id)
            .joins(:poll)
            .joins('left join posts ON posts.id = polls.post_id')
            .where('posts.participe_user_id = ?', politician_id)
            .delete_all
  end
end
