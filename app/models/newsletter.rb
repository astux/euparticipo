class Newsletter < ActiveRecord::Base
  scope :ordered, -> { order('created_at DESC, published_at DESC') } 
  
  validates :published, :subject, :content, presence: true
  
  before_validation :set_default_values
  
  def set_default_values
    self.published = false if self.published.blank?
  end
  
  def publish
    self.published = true
    self.published_at = DateTime.now
    self.save!
  end
end
