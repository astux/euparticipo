class Like < ActiveRecord::Base
  belongs_to :post
  belongs_to :user
  
  include Notifiable
  
  def notification_pack
    if self.post.user_id != self.user_id
      # Avoid spam on constant liking/unliking
      already_liked = UserNotification.where(action_id: UserNotification::LIKE, entity_id: self.post_id)
                                      .where('? = ANY (origin_ids)', self.user_id)
                                      .where('created_at >= ?', DateTime.now - 1.day).exists?
                     
      if !already_liked
        existent = UserNotification.where(action_id: UserNotification::LIKE, entity_id: self.post_id)
                                   .where('created_at >= ?', DateTime.now - 1.day)
                                   .first
        
        if existent
          existent.update_attributes(origin_ids: [self.user_id] | existent.origin_ids)
          []
        else
          [
            {
              recipients: [self.post.user_id],
              entity_id: self.post_id,
              origin_ids: [self.user_id],
              action_id: UserNotification::LIKE
            }
          ]
        end
      else
        []
      end
    end
  end
end
