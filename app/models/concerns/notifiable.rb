module Notifiable
  extend ActiveSupport::Concern
 
  included do
    after_create :notify
  end
  
  def notify
    #begin
      if notification_pack
        notification_pack.each do |n|
          n[:recipients].each do |user_id|
            UserNotification.create!(user_id: user_id, 
                                     action_id: n[:action_id], 
                                     origin_ids: n[:origin_ids], 
                                     entity_id: n[:entity_id])
          end
        end
      end
    #rescue StandardError => e
    #  logger.info("INFO: ACTS_AS_NOTIFIABLE ISSUE > #{e}")
    #end
  end
end
