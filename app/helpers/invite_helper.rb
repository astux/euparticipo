module InviteHelper
  def fb_invite_url(user)
    url = "https://www.facebook.com/dialog/send?app_id=407238639412570"
    url += "&description=EuParticipo é uma rede social que visa reinventar a política brasileira. Junte-se a nós e  
            venha fazer parte dessa mudança!"
    url += "&display=popup"
    url += "&link=http://www.euparticipo.com.br"
    url += "&name=Junte-se a mim no EuParticipo!"
    url += "&picture=#{get_full_url(user.photo.url)}"
    url += "&redirect_uri=#{partials_close_window_url}"
    
    URI.encode(url)
  end
  
  def twttr_invite_url()
    url ="https://twitter.com/intent/tweet"
    url += "?text=Junte-se a mim no EuParticipo! http://www.euparticipo.com.br"
    
    URI.encode(url)
  end
end
