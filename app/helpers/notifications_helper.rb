module NotificationsHelper
  def notification_link_path(n)
    case n.action_id
    when UserNotification::FOLLOW
      user = User.find_by_id n.entity_id
      user_path(user)
    when UserNotification::LIKE
      post = Post.find n.entity_id
      user_post_path(post.user, post)
    when UserNotification::COMMENT
      post = Post.find n.entity_id
      user_post_path(post.user, post)
    when UserNotification::VOTE
      vote = UserVote.find n.entity_id
      user_path(vote.user)
    when UserNotification::ALLIANCE_SUGGESTION
      politician = User.find n.entity_id
      user_path(politician)
    end
  end
  
  def notification_image_path(n)
    case n.action_id
    when UserNotification::FOLLOW
      user = User.find_by_id n.entity_id
      user.photo.thumb.url
    when UserNotification::LIKE
      user = User.find_by_id n.origin_ids.first
      user.photo.thumb.url
    when UserNotification::COMMENT
      user = User.find_by_id n.origin_ids.first
      user.photo.thumb.url
    when UserNotification::VOTE
      vote = UserVote.find n.entity_id
      vote.user.photo.thumb.url
    when UserNotification::ALLIANCE_SUGGESTION
      politician = User.find n.origin_ids.first
      politician.photo.thumb.url
    end
  end
  
  def notification_text(n)
    case n.action_id
    when UserNotification::FOLLOW
      user = User.find_by_id n.entity_id
      if n.user.is_politician? && user.allied?(n.user)
        "<strong>#{user.name}</strong> se aliou a você."
      else
        "<strong>#{user.name}</strong> começou a acompanhar você."
      end
    when UserNotification::LIKE
      user = User.find_by_id n.origin_ids.first
      if n.origin_ids.size() == 1
        "<strong>#{user.name}</strong> curtiu uma publicação sua."
      elsif n.origin_ids.size() == 2
        "<strong>#{user.name}</strong> e mais uma pessoa curtiram uma publicação sua."
      else
        "<strong>#{user.name}</strong> e mais #{n.origin_ids.size()} curtiram uma publicação sua."
      end
    when UserNotification::COMMENT
      user = User.find_by_id n.origin_ids.first
      if n.origin_ids.size() == 1
        "<strong>#{user.name}</strong> comentou em uma publicação sua."
      elsif n.origin_ids.size() == 2
        "<strong>#{user.name}</strong> e mais uma pessoa comentou em uma publicação sua."
      else
        "<strong>#{user.name}</strong> e mais #{n.origin_ids.size()} comentaram em uma publicação sua."
      end
    when UserNotification::VOTE
      vote = UserVote.find n.entity_id
      user = vote.user
      "<strong>#{user.name}</strong> votou em você."
    when UserNotification::ALLIANCE_SUGGESTION
      user = User.find n.origin_ids.first
      politician = User.find n.entity_id
      "<strong>#{user.name}</strong> sugeriu que você se aliasse a 
       <strong>#{politician.display_name} (#{politician.political_position})</strong>."
    end
  end
end
