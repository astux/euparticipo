module PostsHelper
  include AutoHtml
  
  def markdown_post(text)
    auto_html(text) {
      html_escape
      double_quotes
      youtube(width: 400, height: 250, wmode: 'transparent')
      vimeo(width: 400, height: 250)
      link(target: "_blank", rel: "nofollow")
      simple_format
    }
  end
  
  def markdown_comment(text)
    auto_html(text) {
      html_escape
      double_quotes
      link(target: "_blank", rel: "nofollow")
      simple_format
    }
  end
  
  def friendly_time(datetime)
    if Time.now.to_date === datetime.to_date
      datetime.strftime("%H:%M")
    else
      datetime.strftime("%d/%m/%y")
    end
  end
  
  def valid_posts_status
    [Post::STATUS_DEBATE, Post::STATUS_NAO_ADOTADA, Post::STATUS_ADOTADA, Post::STATUS_APROVADA, Post::STATUS_REJEITADA]
  end
  
  def status_label(post, caret = false)
    capture_haml do
      haml_tag 'span.label', class: status_class(post.status) do
        haml_concat status_title(post.status ? post.status : Post::STATUS_DEBATE)
        haml_tag 'b.caret' if caret
      end
    end
  end
  
  def status_title(status)
    case status
    when Post::STATUS_DEBATE then 'Em Debate'
    when Post::STATUS_NAO_ADOTADA then 'Não Adotada'
    when Post::STATUS_ADOTADA then 'Adotada'
    when Post::STATUS_APROVADA then 'Aprovada'
    when Post::STATUS_REJEITADA then 'Rejeitada'
    end
  end
  
  def status_class(status)
    case status
    when Post::STATUS_DEBATE then 'label-warning'
    when Post::STATUS_NAO_ADOTADA then 'label-important'
    when Post::STATUS_ADOTADA then 'label-success'
    when Post::STATUS_APROVADA then 'label-success'
    when Post::STATUS_REJEITADA then 'label-important'
    else 'label-warning'
    end
  end
end
