module AlliedPoliticiansHelper
  def political_positions
    ['Vereador', 'Deputado Estadual', 'Deputado Federal', 'Senador']
  end
end
