module ApplicationHelper
  def get_full_url(suffix)
    return (URI.parse(root_url) + suffix).to_s
  end
end
