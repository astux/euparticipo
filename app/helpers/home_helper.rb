module HomeHelper
  def follows_widget_icon(user)
    capture_haml do
      haml_tag '.photo_friend' do
        haml_tag 'a', {href: user_path(user)} do
          haml_concat image_tag user.get_photo_thumb, title: user.display_name, rel: 'tooltip'
        end
      end
    end
  end
end
