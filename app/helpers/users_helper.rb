module UsersHelper
  def post_type_name(type)
    if type == 'Título e Agraciamento'
      'Homenagens'
    elsif type == 'Emenda Orçamentária'
      'Emendas Orçamentárias'
    else
      type
    end
  end
  
  def right_column_political_position(political_position)
    political_position.gsub(/Deputado/, "<br />Deputado")
  end
end
