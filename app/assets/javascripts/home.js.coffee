$(document).ready ->
  # from
  from_state_select = $('#user-from-state')
  from_city_select = $('#user-from-city')

  $(from_state_select).on 'change', ->
    if this.value == ''
      $(from_city_select).empty().append('<option value="">Selecione um Estado</option>')
    else
      $.getJSON '/states/' + this.value  + '/cities', (data) ->
        $(from_city_select).empty().append '<option value="">Selecione uma cidade</option>'
        $(data).each ->
          $(from_city_select).append '<option value="' + this.id + '">' + this.name + '</option>'
      
  # mandate
  mandate_state_select = $('#user-mandate-state')
  mandate_city_select = $('#user-mandate-city')

  $(mandate_state_select).on 'change', ->
    if this.value == ''
      mandate_city_select.empty().append('<option value="">Selecione um Estado</option>')
    else
      $.getJSON '/states/' + this.value  + '/cities', (data) ->
        $(mandate_city_select).empty().append '<option value="">Selecione uma cidade</option>'
        $(data).each ->
          $(mandate_city_select).append '<option value="' + this.id + '">' + this.name + '</option>'

  # vereador
  $('#user_political_position').on 'change', ->
    if $(this).val() == 'Vereador'
      $('.user_political_city_id').addClass('required').find('label').append(' <abbr title="required">*</abbr>')
    else
      $('.user_political_city_id').removeClass('required').find('label abbr').remove()

  # current
  current_state_select = $('#user-current-state')
  current_city_select = $('#user-current-city')

  $(current_state_select).on 'change', ->
    if this.value == ''
      current_city_select.empty().append('<option value="">Selecione um Estado</option>')
    else
      $.getJSON '/states/' + this.value  + '/cities', (data) ->
        $(current_city_select).empty().append '<option value="">Selecione uma cidade</option>'
        $(data).each ->
          $(current_city_select).append '<option value="' + this.id + '">' + this.name + '</option>'

  # POSTAGEM
  $('#post-form').on 'ajax:before', (event, data, status, xhr) ->
    if $(this).find('textarea').val() == ''
      return false

  # COMENTÁRIO
  
$(document).on 'ready timelineUpdated', ->
  # LIKES
  binds_likes()

  $('.post-coment-box').autosize()
  
  $('.post-text-content').truncate
    max_length: 700
    more: '... ler mais'
    less: 'reduzir'

$(document).on 'ready commentsUpdated', ->
  $('.post-comment-text').truncate
    max_length: 400
    more: '... ler mais'
    less: 'reduzir'

$(document).on 'click', '.post-comment-open-field, .comment-trigger', ->
  post_comment_form = $(this).parent().parent().find('.post-comment-form')
  post_comment_trigger = $(this)
  post_comment_form.fadeIn().find('textarea').focus()
  post_comment_trigger.hide()

# UTILS

# likes

@binds_likes = () ->
  $('.post-like-toggle').off 'ajax:success'
  $('.post-like-toggle').on 'ajax:success', (event, data, status, xhr) ->
    $(this).text(data)