//= require jquery
//= require jquery_ujs
//= require vendor
//= require_tree .

$ ->
  #$('.dropdown-menu').click (e) ->
  #  e.stopPropagation()
  $('.datepicker').datepicker
    format: 'dd/mm/yyyy'
  $('textarea[name="post[content]"]').autosize()
  $('#user_phone1, #user_phone2').inputmask '(99) 99999999[9]', {skipOptionalPartCharacter: " ", placeholder: ' '}
  $('[rel=tooltip]').tooltip()
  $('.chosen').chosen
    placeholder_text: 'Escolha...'
    
  $('.simple-autosize').autosize()

$(document).on 'ajaxStart', ->
  $('#ajax-loading').fadeIn()
  
$(document).on 'ajaxComplete', ->
  $('#ajax-loading').hide()

$(document).on 'change', '#modal-indicar input[type=checkbox]', ->
  $(this).parent().parent().toggleClass('active')

# FB/Twttr Share  
@twttr_click = (a) ->
  u = a.getAttribute('href')
  t = document.title;
  url = "http://twitter.com/intent/tweet?url=#{encodeURIComponent(u)}"
  
  window.open(url, 'Twitter Share', 'toolbar=0,status=0,width=575,height=400')
  $(a).parent().dropdown('close')
  
  return false

@fbs_click = (a) ->
  u = a.getAttribute('href')
  url = "http://www.facebook.com/sharer.php?u=#{encodeURIComponent(u)}"
  
  window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u),'sharer','toolbar=0,status=0,width=626,height=436')
  $(a).parent().dropdown('close')
  
  return false