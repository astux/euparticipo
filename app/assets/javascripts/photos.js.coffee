# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'click', 'a.photo-hook', ->
  $('#modal-photo .modal-loading').show()
  $('#modal-photo .modal-content').hide()
  $('#modal-photo').modal('show')

$(document).on 'ready timelineUpdated', ->
  $('a.photo-zoom').colorbox()
  
$(document).on 'photoModalShown', ->
  $(document).unbind 'keydown'
  $(document).bind 'keydown', (e) ->
    if (e.keyCode == 37)
      # do something when left arrow is pressed
      $('#photo-left')[0].click()
    if (e.keyCode == 39)
      # do something when right arrow is pressed
      $('#photo-right')[0].click()
