$(document).on 'change', 'table.votable td.vote input', ->
  href = $(this).parent().parent().attr('data-href')
  $.ajax
    url: href
    method: 'post'
    type: 'javascript'
  