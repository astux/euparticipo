# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'click', '#speak-hook', ->
  $(this).hide()
  $(this).parent().parent().find('form').show()
  $(this).parent().parent().find('form textarea').focus()

$(document).on 'click', '.post-top-actions .dropdown-toggle', ->
  $(this).tooltip('hide')
