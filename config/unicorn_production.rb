# Set your full path to application.
app_path = "/home/gil/apps/euparticipo"

# Set unicorn options
worker_processes 9
timeout 180
listen 9000

rails_env = ENV['RAILS_ENV'] || 'production'

stderr_path "log/unicorn.err.log"
stdout_path "log/unicorn.out.log"

pid "#{app_path}/tmp/pids/unicorn.pid"
