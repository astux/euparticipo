require "rvm/capistrano"
require "capistrano-unicorn"
require "bundler/capistrano"

set :rvm_ruby_string, '2.0.0'
set :rvm_type, :user

set :application, "EuParticipo"
set :repository,  "git@bitbucket.org:astux/euparticipo.git"

set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`
set :user, "gil"
#set :scm_passphrase, "adicionar passphrase"
ssh_options[:forward_agent] = true

default_run_options[:pty] = true  # Must be set for the password prompt from git to work
set :branch, "master"

set :deploy_via, :remote_cache
set :deploy_to, "/home/gil/apps/euparticipo"
#set :more_share_folders, ["public/uploads","config/database.yml",]
set :shared_children, %w{bin log tmp/pids public/system public/uploads}
#set :linked_files, %w{config/database.yml}

set :use_sudo, false
set :bundle_cmd, 'bundle'
set :keep_releases, 5

role :web, "euparticipo.com.br"                          # Your HTTP server, Apache/etc
role :app, "euparticipo.com.br"                          # This may be the same as your `Web` server
role :db,  "euparticipo.com.br", :primary => true # This is where Rails migrations will run

# if you want to clean up old releases on each deploy uncomment this:
after 'deploy:restart', 'unicorn:restart'  # app preloaded
after 'deploy:update', 'deploy:cleanup'

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end
