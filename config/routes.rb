Euparticipo::Application.routes.draw do
  root 'home#index'
  get 'index-timeline' => 'home#index_timeline', as: 'index_timeline'
  get 'convidar', to: 'invite#index', as: 'invite'

  devise_for :users,
             path: '',
             path_names: { sign_in: 'entrar', sign_up: 'cadastro', sign_out: 'sair' },
             controllers: { registrations: 'users/registrations',
                            confirmations: 'users/confirmations',
                            sessions:      'users/sessions', 
                            omniauth_callbacks: "users/omniauth_callbacks" }

  resources :preferences do
    collection do
      post :update, as: :update
    end
  end
  
  # Rotas de desenvolvimento para testar o mailing
  namespace :dev do
    resources :mailing do
      collection do
        get :confirmacao_cadastro
        get 'digest/:user_id', action: :digest
      end
    end
  end
  
  namespace :admin do
    root :to => 'dashboard#index'
    
    resources :dashboard
    resources :absent_politicians
    resources :newsletters
    resources :pending_politicians do
      member do
        get :approve
        get :deny
      end
    end
  end
  
  get 'novidades', to: 'notifications#index', as: 'notifications'
  
  # Contact
  get 'contato', to: 'about#contact', as: 'contact'
  post 'contact', to: 'about#send_contact', as: 'send_contact'

  get 'sobre', to: 'about#about', as: 'about'
  get 'privacidade', to: 'about#privacy', as: 'privacy'
  get 'termos-de-uso', to: 'about#terms', as: 'terms'
  
  get '/partials/close_window', to: 'partials#close_window', as: 'partials_close_window'

  resources :mock, path: 'mock' do
    collection do
      get :profile
    end
  end

  resources :states do
    resources :cities
  end

  resources :nudges
  resources :politician_invites, path: 'convidar-parlamentar' do
    collection do
      get 'search'
    end
    member do
      get :invite
    end
  end

  resources :allied_politicians, path: 'alie-se' do
    collection do
      get '/:position', action: 'index', as: 'position'
    end
    
    member do 
      get :ally
      get :unally
      get 'recomendar-para/:user_id', as: :recommend, action: :recommend
    end
  end
  
  resources :help, path: 'posso-ajudar', as: "posso_ajudar" do
    collection do
      get 'fale-conosco' => 'help#contact', as: 'contato'
      get 'como-funciona' => 'help#how_it_works', as: 'how_it_works'
    end
  end
  
  get '/:user_id/aliados', to: 'follows#allies', as: :allies
  get '/:user_id/acompanha', to: 'follows#following', as: :following
  get '/:user_id/acompanham', to: 'follows#followers', as: :followers

  resources :users, path: '', :except => [:edit] do
    collection do
      get 'busca' => 'users#search'
      get 'encontrar-amigos' => 'users#search', method: 'discover', as: :find_friends
    end

    member do
      get 'editar-senha'     => 'users#edit_password',   as: 'edit_password'
      post 'update-password' => 'users#update_password', as: 'update_password'

      get 'editar' => 'users#edit', as: 'edit'
      
      get 'biografia/editar' => 'users#edit_biography', as: :edit_biography
      post 'biografia/editar' => 'users#update_biography', as: :update_biography
      get 'biografia' => 'users#biography'
      
      get 'aliados' => 'users#allieds'
      get 'follow' => 'users#follow'
      get 'unfollow' => 'users#unfollow'
      #get 'aliados-a-mim' => 'users#followers', as: 'followers'
      #get 'sou-aliado-a' => 'users#followeds', as: 'followeds'
      get 'participa' => 'users#participa'
      get 'participa-timeline' => 'users#participa_timeline', as: 'participa_timeline'

      get 'dados-pessoais' => 'users#dados_pessoais', as: 'dados_pessoais'
      get 'contatos' => 'users#contatos'
      
      get 'o-que-faz' => 'users#o_que_faz', as: 'o_que_faz'

      get 'participe'                        => 'users#participe'
      get 'participe/:subpagina'             => 'users#participe_subpagina', as: 'participe_subpagina'
      get 'participe/:subpagina/timeline'    => 'users#participe_subpagina_timeline'
      
      get 'nossa-atuacao'                        => 'users#nossa_atuacao', as: 'nossa_atuacao'
      get 'nossa-atuacao/:subpagina'             => 'users#nossa_atuacao_subpagina', as: 'nossa_atuacao_subpagina'
      get 'nossa-atuacao/:subpagina/timeline'    => 'users#nossa_atuacao_subpagina_timeline'
    end
    
    resources :photos, only: [:index, :show]
    resources :user_votes, path: 'votos', as: 'votes'

    resources :posts do
      resources :comments
      get 'like-toggle' => 'posts#like_toggle', as: 'like_toggle'
      member do
        get :likes
        get :status
      end
    end
  end
  
  resources :polls do
    member do
      post 'vote'
    end
  end

  get "*a", :to => "application#routing_error"
end
