# Set your full path to application.
app_path = "/home/gil/apps/euparticipo/current"

# Set unicorn options
worker_processes 6
preload_app true
timeout 180
listen 9000

rails_env = ENV['RAILS_ENV'] || 'production'

stderr_path "log/unicorn.err.log"
stdout_path "log/unicorn.out.log"

pid "#{app_path}/tmp/pids/unicorn.pid"

before_fork do |server, worker|
  ActiveRecord::Base.connection.disconnect!

  old_pid = "#{server.config[:pid]}.oldbin"
  if File.exists?(old_pid) && server.pid != old_pid
    begin
      Process.kill("QUIT", File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
      # someone else did our job for us
    end
  end
end

after_fork do |server, worker|
  ActiveRecord::Base.establish_connection
end
