AutoHtml.add_filter(:double_quotes) do |text|
  text.gsub(/&quot;/, '"')
end