namespace :digest do
  desc 'Test Digest'
  task :test, [:id] => :environment do |t, args|
    user = User.find_by_id args[:id]
    
    DigestMailer.digest(user).deliver
    puts "Enviado para #{user.display_name}"
  end
  
  desc 'Weekly Digest'
  task send: :environment do
    WeeklyDigest.digestables.each do |user|
      #begin
        DigestMailer.digest(user).deliver
        puts "Enviado para #{user.display_name}"
      #rescue
      #  puts "Falha de envio de digest para Usuário ##{user.id}"
      #  Rails.logger.info "Falha de envio de digest para Usuário ##{user.id}"
      #end
    end
  end
end