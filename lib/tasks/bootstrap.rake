#encoding: utf-8
namespace :bootstrap do
  desc "Create some users"
  task :users => :environment do
       gil = User.new({ :email => 'gilmagno@gmailteste.com', :token => 'gilmagno', :state_id => 1, :city_id => 1,
                     :name => 'Gil Magno', :last_names => 'Pereira Cruz',
                     :password => '123123123', :password_confirmation => '123123123',
                     :birthday => '1900-02-01', :confirmed_at => DateTime.now,
                     :role => 'Cidadão', :verified => 't',
                     :political_state_id => 1, :political_city_id => 1,
                     :from_state_id => 1, :from_city_id => 1
        })

       gil.photo = File.open 'gil.jpg'
       gil.save!

       thiago = User.new({ :email => 'pintowar@gmailteste.com',
                     :password => '123123123', :password_confirmation => '123123123',
                     :name => 'Thiago', :last_names => 'Oliveira', :token => 'pintowar',
                     :state_id => 1, :city_id => 1, :confirmed_at => DateTime.now,
                     :role => 'Cidadão', :verified => 't', birthday: '01/01/01'
       })

       thiago.photo = File.open 'thiago.jpg'
       thiago.save!

       party_psol = Party.find_by_name 'PSOL'

       renato = User.new({ :email => 'renatoroseno@gmailteste.com', :phone1 => '12131212', :party => party_psol,
                     :password => '123123123', :password_confirmation => '123123123',
                     :name => 'Renato Roseno', :last_names => 'de Oliveira',
                     :political_nickname => 'Renato Roseno', :token => 'renato-roseno',
                     :confirmed_at => DateTime.now,
                     :role => 'Legislativo', :political_position => 'Deputado Estadual',
                     :verified => 't', birthday: '01/01/01',
                     :political_state_id => 1, :political_city_id => 1
       })

       renato.photo = File.open 'renato.jpg'
       renato.save!

       plinio = User.new({ :email => 'plinioarruda@gmailteste.com', :phone1 => '12131212', :party => party_psol,
                     :password => '123123123', :password_confirmation => '123123123',
                     :name => 'Plínio', :last_names => 'de Arruda',
                     :political_nickname => 'Plínio Sampaio', :token => 'plinio',
                     :confirmed_at => DateTime.now,
                     :role => 'Legislativo', :political_position => 'Senador',
                     :verified => 't', birthday: '01/01/01', :political_nickname => 'Plínio',
                     :political_state_id => 1, :political_city_id => 1
       })

       plinio.photo = File.open 'plinio.jpg'
       plinio.save!

       joao = User.new({ :email => 'joaoalfredo@gmailteste.com', :phone1 => '12131212', :party => party_psol,
                     :password => '123123123', :password_confirmation => '123123123',
                     :name => 'João Alfredo', :last_names => 'Telles Melo',
                     :political_nickname => 'João Alfredo',
                     :confirmed_at => DateTime.now, :token => 'joao-alfredo',
                     :role => 'Legislativo', :political_position => 'Vereador',
                     :verified => 't', birthday: '01/01/01', :political_nickname => 'João Alfredo',
                     :political_state_id => 1, :political_city_id => 1
       })

       joao.photo = File.open 'joao.jpg'
       joao.save!

       soraya = User.new({ :email => 'soraya@gmailteste.com', :phone1 => '12131212', :party => party_psol,
                     :password => '123123123', :password_confirmation => '123123123',
                     :name => 'Soraya', :last_names => 'Tupinambá',
                     :political_nickname => 'Soraya',
                     :confirmed_at => DateTime.now, :token => 'soraya',
                     :role => 'Legislativo', :political_position => 'Senador',
                     :verified => 't', birthday: '01/01/01', :political_nickname => 'Soraya Tupinambá',
                     :political_state_id => 1, :political_city_id => 1
       })

       soraya.photo = File.open 'soraya.jpg'
       soraya.save!

       gonzaga = User.new({ :email => 'gonzaga@gmailteste.com', :phone1 => '12131212', :party => party_psol,
                     :password => '123123123', :password_confirmation => '123123123',
                     :name => 'Francisco', :last_names => 'Gonzaga',
                     :political_nickname => 'Gonzaga',
                     :confirmed_at => DateTime.now, :token => 'gonzaga',
                     :role => 'Legislativo', :political_position => 'Vereador',
                     :verified => 't', birthday: '01/01/01', :political_nickname => 'Gonzaga',
                     :political_state_id => 1, :political_city_id => 1
       })

       gonzaga.photo = File.open 'gonzaga.jpg'
       gonzaga.save!

       guilherme = User.new({ :email => 'guilherme@gmailteste.com', :phone1 => '12131212', :party => party_psol,
                     :password => '123123123', :password_confirmation => '123123123',
                     :name => 'Guilherme', :last_names => 'Sampaio',
                     :political_nickname => 'Guilherme',
                     :confirmed_at => DateTime.now, :token => 'guilherme',
                     :role => 'Legislativo', :political_position => 'Vereador',
                     :verified => 't', birthday: '01/01/01', :political_nickname => 'Guilherme Sampaio',
                     :political_state_id => 1, :political_city_id => 1
       })

       guilherme.photo = File.open 'guilherme.jpg'
       guilherme.save!

       luizianne = User.new({ :email => 'luizianne@gmailteste.com', :phone1 => '12131212', :party => party_psol,
                     :password => '123123123', :password_confirmation => '123123123',
                     :name => 'Luizianne', :last_names => 'Lins',
                     :political_nickname => 'Luizianne',
                     :confirmed_at => DateTime.now, :token => 'luizianne',
                     :role => 'Legislativo', :political_position => 'Vereador',
                     :verified => 't', birthday: '01/01/01', :political_nickname => 'Luizianne',
                     :political_state_id => 1, :political_city_id => 1
       })

       luizianne.photo = File.open 'luizianne.jpg'
       luizianne.save!

       jean = User.new({ :email => 'jean@gmailteste.com', :phone1 => '12131212', :party => party_psol,
                     :password => '123123123', :password_confirmation => '123123123',
                     :name => 'Jean', :last_names => 'Wyllis',
                     :political_nickname => 'Jean',
                     :confirmed_at => DateTime.now, :token => 'jean',
                     :role => 'Legislativo', :political_position => 'Deputado Federal',
                     :verified => 't', birthday: '01/01/01', :political_nickname => 'Jean Wyllis',
                     :political_state_id => 1, :political_city_id => 1
       })

       jean.photo = File.open 'jean.jpg'
       jean.save!

       Follow.create({ :follower_id => 1, :followed_id => 5 })
       Follow.create({ :follower_id => 2, :followed_id => 5 })
       Follow.create({ :follower_id => 3, :followed_id => 5 })
       Follow.create({ :follower_id => 4, :followed_id => 5 })

       Alliance.create({ :citizen_id => 1, :politician_id => 3 })
  end

  desc "Create some posts"
  task :posts => :environment do
       plinio = User.find_by_token! 'plinio'
       joaoalfredo = User.find_by_token! 'joao-alfredo' 
       gil = User.find_by_token! 'gil-magno'

       post = Post.create( user: gil, content: 'Conteúdo do post' )
       Comment.create( post: post, user: gil, content: 'Conteúdo do comentário' )
       Comment.create( post: post, user: gil, content: 'Conteúdo de qwe comentário' )
       Comment.create( post: post, user: gil, content: 'Conteúdo de qwe comentário' )
       Comment.create( post: post, user: gil, content: 'Conteúdo de qwe comentário' )

       ['Voto', 'Lei', 'Discurso', 'Denúncia', 'Audiência Pública', 'Requerimento', 'Título e Agraciamento', 'Emenda Orçamentária'].each do |t|
         (1..50).each do
           Post.create( user: gil, participe_user_id: joaoalfredo.id, content: 'Post em Voto do João de Alfredo', post_type: t )
         end
       end

       Post.create( user: plinio, participe_user_id: plinio.id, content: 'Post em Voto do Plínio', post_type: 'Em Ação' )
       Post.create( user: plinio, participe_user_id: plinio.id, content: 'Post em Voto do Plínio', post_type: 'Proposições' )
       Post.create( user: plinio, participe_user_id: plinio.id, content: 'Post em Voto do Plínio', post_type: 'Na Mídia' )
       Post.create( user: plinio, participe_user_id: plinio.id, content: 'Post em Voto do Plínio', post_type: 'Prestação de Contas' )
       Post.create( user: plinio, participe_user_id: plinio.id, content: 'Post em Voto do Plínio', post_type: 'Artigos e Publicações' )
       Post.create( user: plinio, participe_user_id: plinio.id, content: 'Post em Voto do Plínio', post_type: 'Agenda' )
  end

  desc "Run all bootstrapping tasks"
  task :all => [:users, :posts]
end